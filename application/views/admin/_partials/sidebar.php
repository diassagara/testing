<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin/Overview') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Overview</span>
        </a>
    </li>
    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'products' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="<?php echo site_url('admin/products') ?>" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-boxes"></i>
            <span>Services</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/products/add') ?>">New Services</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/products') ?>">List Services</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/Orders') ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Orders</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/Detail-service') ?>">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Detail Service</span></a>
    </li>
   <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'articles' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="<?php echo site_url('admin/articles') ?>" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-book"></i>
            <span>Articles</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/articles/add') ?>">New article</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/articles') ?>">List article</a>
        </div>
    </li>
      <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'gallerys' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="<?php echo site_url('admin/gallerys') ?>" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-image"></i>
            <span>Gallery</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/gallerys/add') ?>">New gallery</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/gallerys') ?>">List gallery</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/Setting') ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span></a>
    </li>
</ul>
