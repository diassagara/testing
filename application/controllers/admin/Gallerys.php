<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallerys extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("gallery_model");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["gallerys"] = $this->gallery_model->getAll();
        $this->load->view("admin/gallery/list", $data);
    }

    public function add()
    {
        $gallery = $this->gallery_model;
        $validation = $this->form_validation;
        $validation->set_rules($gallery->rules());

        if ($validation->run()) {
            $gallery->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/gallery/new_form");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/gallerys');
       
        $gallery = $this->gallery_model;
        $validation = $this->form_validation;
        $validation->set_rules($gallery->rules());

        if ($validation->run()) {
            $gallery->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["gallerys"] = $gallery->getById($id);
        if (!$data["gallerys"]) show_404();
        
        $this->load->view("admin/gallery/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->gallery_model->delete($id)) {
            redirect(site_url('admin/gallerys'));
        }
    }
}