<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $_table = "services";

    public $service_id;
    public $nama_service;
    public $harga;
    public $description;

    public function rules()
    {
        return [
            ['field' => 'nama_service',
            'label' => 'Nama_service',
            'rules' => 'required'],

            ['field' => 'harga',
            'label' => 'Harga',
            'rules' => 'numeric'],
            
            ['field' => 'description',
            'label' => 'Description',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["service_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->service_id = uniqid();
        $this->nama_service = $post["nama_service"];
        $this->harga = $post["harga"];
        $this->description = $post["description"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->service_id = $post["id"];
        $this->nama_service = $post["nama_service"];
        $this->harga = $post["harga"];
        $this->description = $post["description"];
        return $this->db->update($this->_table, $this, array('service_id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("service_id" => $id));
    }
}