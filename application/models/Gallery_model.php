<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model
{
    private $_table = "gallerys";

    public $gallery_id;
    public $nama_gallery;
    public $image = "default.jpg";
    

    public function rules()
    {
        return [
            ['field' => 'nama_gallery',
            'label' => 'Nama_gallery',
            'rules' => 'required']

           
            
           
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["gallery_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->gallery_id = uniqid();
        $this->nama_gallery = $post["nama_gallery"];
        //$this->image = $post["image"];
        $this->image = $this->_uploadImage();
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->gallery_id = $post["id"];
        $this->nama_gallery = $post["nama_gallery"];
        //$this->image = $post["image"];

        if (!empty($_FILES["image"]["name"])) {
    $this->image = $this->_uploadImage();
} else {
    $this->image = $post["old_image"];
}




        return $this->db->update($this->_table, $this, array('gallery_id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("gallery_id" => $id));
    }


    private function _uploadImage()
{
    $config['upload_path']          = './upload/image-gallery/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['file_name']            = $this->gallery_id;
    $config['overwrite']            = true;
    $config['max_size']             = 10024; // 1MB
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('image')) {
        return $this->upload->data("file_name");
    }
    
    return "default.jpg";
}


    private function _deleteImage($id)
{
    $gallery = $this->getById($id);
    if ($gallery->image != "default.jpg") {
        $filename = explode(".", $gallery->image)[0];
        return array_map('unlink', glob(FCPATH."upload/image-gallery/$filename.*"));
    }
}
}